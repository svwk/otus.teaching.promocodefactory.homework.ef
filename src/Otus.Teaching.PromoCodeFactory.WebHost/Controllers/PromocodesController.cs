﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Employee> _employeesRepository;


        public PromocodesController(IRepository<PromoCode> promoCodesRepository, 
            IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository, IRepository<Employee> employeesRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _employeesRepository = employeesRepository;
        }

        /// <summary>
        /// все промокоды
        /// </summary>
        /// <returns>Список в формате PromoCodeShortResponse </returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var preferences = await _promoCodesRepository.GetAllAsync();

            var promocodes = preferences.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToString("dd-MM-yyyy"),
                EndDate = x.EndDate.ToString("dd-MM-yyyy")
                
            }).ToList();

            return Ok(promocodes);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns>Guid промокода</returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferencesRepository.GetFirstByIdAsync(request.Preference);

            var customers = await _customersRepository.GetManyByCondition(x =>
                x.CustomerPreferences.Count(y => y.PreferenceId == request.Preference)>0);

            var employee =await _employeesRepository.GetFirstByCondition(x => x.Role.Name == "PartnerManager");


            PromoCode promoCode = PromoCodeMapper.MapGromModel(request, preference, customers);
            promoCode.EmployeeId = employee.Id;
            promoCode.PartnerManager = employee;

            await _promoCodesRepository.AddAsync(promoCode);

            return Ok(promoCode.Id);
        }
    }
}