﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext
        : DbContext
    {
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }        
        
        public DbSet<Partner> Partners { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }
        
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<PartnerPromoCodeLimit> PartnerPromoCodeLimits { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DbSet<CustomerPromoCode> CustomerPromoCodes { get; set; } 


        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(bc => new { bc.CustomerId, bc.PreferenceId});


            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.CustomerPreferences)
                .HasForeignKey(bc => bc.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany(b=>b.CustomerPreferences)
                .HasForeignKey(bc => bc.PreferenceId);


            modelBuilder.Entity<CustomerPromoCode>()
                .HasKey(bc => new { bc.CustomerId, bc.PromoCodeId });


            modelBuilder.Entity<CustomerPromoCode>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.CustomerPromoCodes)
                .HasForeignKey(bc => bc.CustomerId);

            modelBuilder.Entity<CustomerPromoCode>()
                .HasOne(bc => bc.PromoCode)
                .WithMany(b => b.CustomerPromoCodes)
                .HasForeignKey(bc => bc.PromoCodeId);

            
            //modelBuilder.Entity<MainPromoCodePartnerPreference>().ToTable("MainPromoCodePartnerPreferences");

            //modelBuilder.Entity<PartnerPromoCodeLimit>().ToTable("PartnerPromoCodeLimits");
        }
    }
}