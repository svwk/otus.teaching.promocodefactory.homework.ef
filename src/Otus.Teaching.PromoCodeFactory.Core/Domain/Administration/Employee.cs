﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [MaxLength(30)]
        public string FirstName { get; set; }
        
        [MaxLength(30)]
        public string LastName { get; set; }

        [MaxLength(60)]
        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(20)]
        public string Email { get; set; }

        public Guid RoleId { get; set; }
        
        public virtual Role Role { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}