﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(30)]
        public string FirstName { get; set; }

        [MaxLength(30)]
        public string LastName { get; set; }

        public string SecondName { get; set; }

        [MaxLength(60)]
        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(30)]
        public string Email { get; set; }

        // Списки Preferences и Promocodes 

        public virtual ICollection<CustomerPromoCode> CustomerPromoCodes { get; set; }

        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }

        
    }
}